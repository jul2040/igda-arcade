#!/usr/bin/python3

import time
from pynput.keyboard import Key, Listener
from subprocess import Popen
from subprocess import TimeoutExpired
from sys import argv

last_pressed = time.time()
def reset_timer(_key):
	global last_pressed
	last_pressed = time.time()

last_pressed = time.time()
if(len(argv) > 1):
	command = ""
	for i in range(1, len(argv), 1):
		command += argv[i] + " "
	print("running ", command)
	process = Popen("exec " + command, shell=True)
else:
	print("error: missing run argument")
	quit()
try:
	listener = Listener(on_press=reset_timer, on_release=reset_timer)
	listener.start()
	while True:
		if(time.time()-last_pressed > 3*60): # check if its been more than 3 minutes since we last interacted with the arcade
			# if it has, kill the game
			print("Inactive, killing subprocess")
			process.kill()
			break
		try:
			process.communicate(timeout=1) # check if its still running, with a timeout of 1 second
			break
		except TimeoutExpired:
			#the process is still running
			pass
except KeyboardInterrupt:
	pass
