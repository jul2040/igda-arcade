extends Control

func get_relative_path(folder_name:String, file_name:String):
	return "./games/"+folder_name+"/"+file_name

func _on_Generate_pressed():
	if($Exec.text == ""):
		$Exec.grab_focus()
		print("you need an executable")
		return
	if($Name.text == ""):
		$Name.grab_focus()
		print("your game needs a folder name")
		return
	var data = {}
	if($Title.text != ""):
		data["title"] = $Title.text
	var exec:Array = $Exec.text.split(" ", false)
	if(len(exec) == 0):
		$Exec.grab_focus()
		print("you need an executable")
		return
	data["exec_path"] = exec[0]
	if(len(exec) > 1):
		data["exec_args"] = exec.slice(1, -1)
	data["name"] = $Name.text
	if($Banner.text != ""):
		data["banner_img"] = $Banner.text
	if($Video.text != ""):
		data["video"] = $Video.text
	if($BGImg.text != ""):
		data["bg_img"] = $BGImg.text
	if($About.text != ""):
		data["about"] = $About.text
	var json:String = JSON.print(data)
	print(json)
	var dir := Directory.new()
	dir.open("./games/")
	dir.make_dir(data["name"])
	var file:File = File.new()
	file.open(get_relative_path(data["name"], "game.json"), File.WRITE)
	file.store_string(json)
	file.close()

func _input(event):
	if(event.is_action_pressed("swap_mode")):
		get_tree().change_scene("res://Arcade.tscn")
