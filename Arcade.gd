extends Control

func get_games():
	var data = []
	var dir: = Directory.new()
	dir.open("./games")
	dir.list_dir_begin(true, true)
	var file_name: = dir.get_next()
	while(file_name != ""):
		if(dir.current_is_dir()):
			var file = File.new()
			file.open("./games/%s/game.json"%file_name, File.READ)
			var string:String = ""
			if(file.get_error() == OK):
				string = file.get_as_text()
				file.close()
				var json = JSON.parse(string)
				if(json.error == OK):
					data.append(json.result)
				else:
					print("Error parsing ./games/%s/game.json"%file_name)
			else:
				print("Error opening ./games/%s/game.json"%file_name)
		file_name = dir.get_next()
	return data

var games = []
func _ready():
	refresh_games()

func refresh_games():
	for game in $Games.get_children():
		game.queue_free()
	games = []
	var data = get_games()
	for i in range(len(data)):
		var game = preload("res://Game.tscn").instance()
		game.connect("add_trailer", $Intro, "add_trailer")
		game.anchor_left = i
		game.anchor_right = i+1
		game.set_data(data[i])
		games.append(game)
		$Games.add_child(game)
	$Intro._on_InactivityTimer_timeout()

var cur_game = 0
func _process(_delta):
	if(not active):
		return
	if(Input.is_action_just_pressed("ui_right")):
		set_game(cur_game+1)
	elif(Input.is_action_just_pressed("ui_left")):
		set_game(cur_game-1)

var animation_speed:float = 0.3
var animating:bool = false
var prev_game:int = 0
func set_game(g:int):
	if(g < 0 or g >= len(games) or animating):
		return
	animating = true
	$Tween.interpolate_property($Games, "anchor_left", $Games.anchor_left, -g, animation_speed, Tween.TRANS_QUART, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($Games, "anchor_right", $Games.anchor_right, -g+1, animation_speed, Tween.TRANS_QUART, Tween.EASE_IN_OUT)
	$Tween.start()
	prev_game = cur_game
	cur_game = g

func _on_Tween_tween_all_completed():
	animating = false
	games[prev_game].unfocus()
	games[cur_game].focus()

var active = false
func focus_game():
	active = true
	games[cur_game].focus()

func unfocus_game():
	active = false
	games[cur_game].unfocus()

func _input(event):
	if(event.is_action_pressed("swap_mode")):
		get_tree().change_scene("res://GameCreator.tscn")
