extends Node

"""
This script will save data to a file, and retrieve it from a file.
Intended to be used as a Singleton.
The data must be in key/value pairs.
To set/get data, call get(key) or set(key, value) from somewhere else.
"""

# filename in user://
export(String) var save_file:String = "game_data.json"
# will save to the file every time you set some data.
# may write a lot depending on application, use with caution.
# to avoid many disk writes,
# prefer manually calling save_to_file() only when needed.
export(bool) var save_on_set:bool = true
# automatically read data from file on startup.
export(bool) var load_on_ready:bool = true

var data = {
		"play_time":{},
		"avg_playtime":{},
		"plays":{}
		}
func set(key, value):
	data[key] = value
	if(save_on_set):
		save_to_file()

func get(key):
	return data[key]

func _ready():
	if(load_on_ready):
		load_from_file()

# this will run when the game exits.
# in html5, it may not run, save_on_write = true may be needed.
func _exit_tree():
	save_to_file()

func save_to_file():
	var dump:String = JSON.print(data) # dump data to a string
	var f:File = File.new()
	var err:int = f.open("./%s"%save_file, File.WRITE) # open for writing
	if(err == OK):
		f.store_string(dump)
		f.close()
	else:
		print("There was a problem saving data to a file.\nError #%d"%err)

func load_from_file():
	var f:File = File.new()
	var err:int = f.open("user://%s"%save_file, File.READ)
	if(err == OK):
		var text:String = f.get_as_text()
		f.close()
		var new:JSONParseResult = JSON.parse(text)
		if(new.error == OK):
			for k in new.result.keys():
				data[k] = new.result[k]
		else:
			print("Invalid JSON in save file.")
			print(new.error_string)
			save_to_file() # overwrite invalid json data.
	else:
		print("There was a problem loading data from a file\nError #%d"%err)
