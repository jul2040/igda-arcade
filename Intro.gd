extends Control

var intro_visible:bool = true

func check_activity():
	return (Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("ui_left")
			or Input.is_action_just_pressed("ui_right") or Input.is_action_just_pressed("ui_up")
			or Input.is_action_just_pressed("ui_cancel") or Input.is_action_just_pressed("ui_select"))

func _process(_delta):
	if(not intro_visible):
		$PlayVideoTimer.start()
	if(not intro_visible and check_activity()):
		$InactivityTimer.start() # restart the timer, prolonging the inactivity state
	if(not video_playing and intro_visible and check_activity()):
		$AnimationPlayer.play("swipe_out")
		intro_visible = false
		owner.focus_game()
		$InactivityTimer.start()
	if(video_playing and check_activity()):
		$VideoPlayer.stream = null
		$PlayVideoTimer.start()
		video_playing = false

func _on_InactivityTimer_timeout():
	$AnimationPlayer.play("swipe_in")
	intro_visible = true
	owner.unfocus_game()

var video_playing = false
func _on_PlayVideoTimer_timeout():
	if(len(trailers) == 0):
		return
	$Black/AnimationPlayer.play("fade_out")

func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "fade_out"):
		$VideoPlayer.stream = trailers[randi()%len(trailers)]
		$VideoPlayer.play()
		video_playing = true
		$Black/AnimationPlayer.play("fade_in")

func _on_VideoPlayer_finished():
	$VideoPlayer.stop()
	video_playing = false
	$VideoPlayer.stream = null
	$PlayVideoTimer.start()

var trailers:Array = []
func add_trailer(stream):
	trailers.append(stream)
