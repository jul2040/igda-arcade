extends Control

var focused = false
func focus():
	focused = true
	$VideoPlayer.play()

func unfocus():
	focused = false
	$VideoPlayer.stop()

func _input(event):
	if(not focused):
		return
	for action in ["ui_up", "ui_down", "ui_right", "ui_left", "ctrl", "alt", "shift"]:
		if(event.is_action(action)):
			return
	if(event is InputEventKey and event.pressed):
		# remove fullscreen and wait a frame
		#OS.window_fullscreen = false
		yield(get_tree(), "idle_frame")
		
		var output = []
		
		var start_time:float = OS.get_system_time_msecs()
		if(OS.get_name() == "X11"):
			var args = []
			if(exec_prefix != ""):
				args.append(exec_prefix)
			args.append(get_relative_path(exec_path))
			args.append_array(exec_args)
			OS.execute("./launch_kill.py", args, true, output) # will block the godot thread until this is done
		elif(OS.get_name() == "Windows"):
			var args = []
			args.append_array(exec_args)
			if(exec_prefix == ""):
				OS.execute(get_relative_path(exec_path), args, true, output) # will block the godot thread until this is done
			else:
				args.push_front(get_relative_path(exec_path))
				OS.execute(exec_prefix, args, true, output) # will block the godot thread until this is done
		else:
			print("Unknown OS")
		var time_elapsed:float = OS.get_system_time_msecs()-start_time
		
		# save metrics
		# avg playtime
		var avg_playtime = Serializer.get("avg_playtime")
		var plays = Serializer.get("plays")
		if(not avg_playtime.has(data["name"])):
			avg_playtime[data["name"]] = time_elapsed
		else:
			var p = plays[data["name"]]
			avg_playtime[data["name"]] = ((avg_playtime[data["name"]])*p+time_elapsed)/(p+1)
		Serializer.set("avg_playtime", avg_playtime)
		# playtime
		var play_time = Serializer.get("play_time")
		if(not play_time.has(data["name"])):
			play_time[data["name"]] = time_elapsed
		else:
			play_time[data["name"]] += time_elapsed
		Serializer.set("play_time", play_time)
		# plays
		if(not plays.has(data["name"])):
			plays[data["name"]] = 1
		else:
			plays[data["name"]] += 1
		Serializer.set("plays", plays)
		
		print("Process output: ", output)
		
		yield(get_tree(), "idle_frame")
		
		get_node("../..").refresh_games()
		
		# in case the game removed the launcher's fullscreen status
		OS.window_fullscreen = true

var exec_prefix = ""
var exec_path = ""
var exec_args = []
var data = {}

func get_relative_path(file_name:String):
	return "./games/"+data["name"]+"/"+file_name

signal add_trailer(stream)
func set_data(d:Dictionary):
	data = d
	exec_path = data["exec_path"]
	if(data.has("title")):
		$Label.text = data["title"]
	if($Label.text == ""):
		$Label.visible = false # hide the black box if there is no game title
	if(data.has("banner_img")):
		var img := Image.new()
		var err:int = img.load(get_relative_path(data["banner_img"]))
		if(err == OK):
			var tex := ImageTexture.new()
			tex.create_from_image(img)
			$BannerRect.texture = tex
	if(data.has("exec_args")):
		exec_args = data["exec_args"]
	if(data.has("exec_prefix")):
		exec_prefix = data["exec_prefix"]
	if(data.has("video")):
		var video:VideoStreamTheora = VideoStreamTheora.new()
		video.set_file(get_relative_path(data["video"]))
		$VideoPlayer.stream = video
		if($VideoPlayer.stream != null):
			emit_signal("add_trailer", $VideoPlayer.stream)
	if(data.has("bg_img")):
		var img := Image.new()
		var err:int = img.load(get_relative_path(data["bg_img"]))
		if(err == OK):
			var tex := ImageTexture.new()
			tex.create_from_image(img)
			$TextureRect.texture = tex
	if(data.has("about")):
		$About/Label2.text = data["about"]
	else:
		$About.visible = false

func _on_VideoPlayer_finished():
	if(focused):
		$VideoPlayer.play()
